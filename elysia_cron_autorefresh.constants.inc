<?php
/**
 * @file
 *   Constants for the "Elysia Cron Auto-refresh" module.
 *
 *   © 2018 Red Bottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Paddock (guy@redbottledesign.com)
 */

/**
 * The machine name for this module.
 *
 * @var string
 */
define('ELCRAU_MODULE_NAME', 'elysia_cron_autorefresh');

/**
 * The path to the JS file that accompanies the module, relative to the module
 * path.
 *
 * @var string
 */
define('ELCRAU_JS_PATH', 'js/elysia_cron_autorefresh.js');

/**
 * The HTML ID for the div around the status page content.
 *
 * @var string
 */
define('ELCRAU_WRAPPER_ID', 'cron-status');

/**
 * The name of the variable that controls how often the status page refreshes.
 *
 * The interval is specified in milliseconds.
 *
 * At the moment, this setting has no admin interface. It is provided only for
 * the benefit of site admins who want to tune the frequency of updates to
 * a different interval other than 1000 milliseconds (i.e. 1 second).
 *
 * @var string
 */
define('ELCRAU_VAR_REFRESH_INTERVAL', 'elysia_cron_autorefresh_interval');
