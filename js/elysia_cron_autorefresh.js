/**
 * @file
 *   JavaScript behaviors for the "Elysia Cron Auto-refresh" module.
 *
 *   © 2018 Red Bottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Paddock (guy@redbottledesign.com)
 */
(function($) {
  /**
   * Extension to the Drupal ajax object to allow triggering of an AJAX response
   * without an element having to trigger it.
   *
   * Adapted from:
   * https://www.deeson.co.uk/labs/trigger-drupal-managed-ajax-calls-any-time-drupal-7
   */
  Drupal.ajax.prototype.performRequest = function() {
    if (!this.ajaxing) {
      try {
        return $.ajax(this.options);
      } catch (error) {
        return console.error(
          'An error occurred while attempting to process "' + this.options.url
          + '": ' + error);
      }
    }
  };

  /**
   * Contains all of the logic necessary to add an auto-refresh callback to the
   * Elysia Cron status page and perform the callback on a schedule.
   */
  Drupal.behaviors.elysiaCronAutorefresh = {
    REFRESH_ENDPOINT: 'admin/config/system/cron/ajax/refresh',

    /**
     * Script attachment callback.
     *
     * This triggers a refresh of the status page as soon as the auto-refresh
     * interval has expired. Since Drupal calls this method after every AJAX
     * request has completed, this ensures that only one request is pending at
     * a time, in case the server falls behind.
     */
    attach: function (context, settings) {
      this.instanceSetTimeout(
        this.refresh,
        Drupal.settings.elysiaCronAutorefresh.interval);
    },

    /**
     * Performs a single refresh of the Elysia Cron status.
     */
    refresh: function(completeCallback) {
      var request = {
        url:      this.getRefreshUrl(),
        event:    'onload',
        keypress: false,
        prevent:  false
      };

      new Drupal.ajax(null, $(document.body), request).performRequest();
    },

    /**
     * Invoke the specified instance method on this object after the specified
     * delay.
     *
     * @param {function} method
     *   A reference to the function on this object to be invoked. It is
     *   automatically bound to this object before being invoked.
     * @param {int} delay
     *   The delay (in milliseconds) to wait before firing the method.
     */
    instanceSetTimeout: function(method, delay) {
      (function(_this, _method, _delay) {
        var callback = function() {
          _method.apply(_this);
        };

        setTimeout(callback, _delay);
      })(this, method, delay);
    },

    /**
     * Gets the URL on the backend that provides the updated info as AJAX
     * commands.
     *
     * @returns {string}
     *   The URL for the AJAX refresh endpoint (including the site base path, as
     *   needed).
     */
    getRefreshUrl: function() {
      return Drupal.settings.basePath + this.REFRESH_ENDPOINT;
    }
  };
})(jQuery);
