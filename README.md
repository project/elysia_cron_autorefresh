# Elysia Cron Auto-refresh
**Author:** Guy Elsmore-Paddock (as part of the Arkayo project for Inveniem, LLC)

A simple module that enhances the Elysia Cron status page 
(`/admin/config/system/cron`) with logic to pull and display the latest cron
status every few seconds without the user having to refresh the page manually.
The status is updated in such a way that the user's scroll position on the page 
should not be affected.

## Installation
This module depends on Elysia Cron. Install it first, then you can install this 
module.

## Adjusting the Update Frequency
The default update frequency is 1 second. This should be frequent enough to
provide nearly real-time status of the cron queue, but may be problematic on
large sites with lots of cron tasks or Elysia Cron channels.

There is no admin interface for adjusting the update frequency, but it can be 
tuned by manually setting the `elysia_cron_autorefresh_interval` variable in 
the Drupal `variables` table to the desired interval, in milliseconds. If there 
is enough demand for making this variable configurable, it may become so in a 
future version.
